import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { API_CONFIG_TOKEN } from '../config/api.config';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  baseUrl = environment.base_url;

  constructor(private http: HttpClient, @Inject(API_CONFIG_TOKEN) private apiConfig) { }

  getShowList(): Observable<any> {
    const url = `${this.baseUrl}${this.apiConfig.SHOWS}`;
    return this.http.get(url);
  }

  getSeasonsByShowId(): Observable<any> {
    const url = `${this.baseUrl}/${this.apiConfig.SHOW_EPISODE_LIST}`;
    return this.http.get(url);
  }

  getShowCast(): Observable<any> {
    const url = `${this.baseUrl}/${this.apiConfig.SHOW_CAST}`;
    return this.http.get(url);
  }

  getShowCrew(): Observable<any> {
    const url = `${this.baseUrl}/${this.apiConfig.SHOW_CREW}`;
    return this.http.get(url);
  }

  getShowGallery(): Observable<any> {
    const url = `${this.baseUrl}/${this.apiConfig.SHOW_IMAGES}`;
    return this.http.get(url);
  }
}
