import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { SharedService } from '../shared/services/shared.service';

@Component({
  selector: 'app-shows',
  templateUrl: './shows.component.html',
  styleUrls: ['./shows.component.css']
})
export class ShowsComponent implements OnInit {

  popularShows$: Observable<any>;

  constructor(private service: SharedService) { }

  ngOnInit(): void {
    this.popularShows$ = this.getPopularShows();
  }

  getPopularShows(): Observable<any> {
    return this.service.getShowList().pipe(
        filter(show => {
          console.log(show);
          return show;
        })
    );
  }

}
