import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ShowsComponent } from './shows/shows.component';
import { API_CONFIG, API_CONFIG_TOKEN } from './shared/config/api.config';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ShowsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: API_CONFIG_TOKEN,
      useValue: API_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
